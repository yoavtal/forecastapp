var geocoder;
var tableHistory;

window.onload = function () { 
    
    geocoder = new google.maps.Geocoder();
    tableHistory = document.getElementById("history");
         
    $.getJSON('https://geoip-db.com/json/geoip.php?jsonp=?') 
         .done (function(location)
         {
             var URL = "http://forecast.io/#/f/" + location.latitude + "," + location.longitude;   
             createFrame(URL);     
         });

   initialize();
}

function createFrame(URL){  
     var searchCriteria = document.getElementById('searchcriteria');
     var newIframe = document.createElement('iframe');
     newIframe.id="forecast_embed";
     newIframe.type="text/html";
     newIframe.frameborder="0";
     newIframe.height="500";
     newIframe.width="100%";
     newIframe.src = URL;  
     searchCriteria.appendChild(newIframe);
}

function initialize(){
    $(document).keypress(function(event) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
           var city_name = document.getElementById("city_name").value;
           if(city_name.length == 0){
               alert("Please enter a location");
           }
           else{
                geocoder.geocode( { 'address': city_name}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var latitude = results[0].geometry.location.lat();
                    var longitude = results[0].geometry.location.lng();                   
                    var URL = "http://forecast.io/#/f/" + latitude + "," + longitude;
                    var element = document.getElementById("forecast_embed");
                    element.parentNode.removeChild(element);
                    createFrame(URL);
                    var d = new Date();
                    var datestring = d.getDate()  + "-" + (d.getMonth()+1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes();
                    document.cookie = city_name + "=" + datestring;
                } 
                else{
                    alert("Could not find location");
                }
                }); 
           }
        }
    });
}

function ReadCookies(){
   $("#history tr").remove();
   var allcookies = document.cookie;
   cookiearray  = allcookies.split(';');
   for(var i=0; i<cookiearray.length; i++){
      key = cookiearray[i].split('=')[0];
      value = cookiearray[i].split('=')[1];
       var row = tableHistory.insertRow(i);
       var city = row.insertCell(0);
       var date = row.insertCell(1);
       city.innerHTML = key;
       date.innerHTML = value;
   }  
}